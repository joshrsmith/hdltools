'''
(c) 2015 Joshua Smith

Created on Aug 30, 2015
'''

import argparse
from hdltools.compile_order import get_compile_order_source_files


def main():
    parser = argparse.ArgumentParser(
        description=('Generate a compile order CSV by recursively including '
                     'VHDL files from top level libraries'))
    parser.add_argument('root_path',
                        help='Root path where the libraries are located')
    parser.add_argument('libraries', nargs='+',
                        help='libraries to include')
    parser.add_argument('csv_file',
                        help='Output compile-order csv file')

    args = parser.parse_args()

    source_files = get_compile_order_source_files(
        args.root_path, args.libraries, target=None)

    with open(args.csv_file, 'w') as csv_fh:
        for source_file in source_files:
            csv_fh.write(
                source_file.library.name + ", " + source_file.name + "\n")


if __name__ == '__main__':
    main()
