'''
(c) 2015 Joshua Smith

Created on Aug 30, 2015
'''

import subprocess
from os.path import basename, splitext

from hdltools.reporter import ActionResult
from hdltools.project import get_source_file_libraries
from hdltools.process import Process


def get_process_info_string(process):
    """Return a string summarizing execution info of a process"""
    return process.arg_string + "\n" + process.output


class Riviera(object):
    """Toolchain for interacting with Riviera-PRO software"""
    TOOL_NAME = "riviera"

    def __init__(self, workingdir, globalcompargs=None):
        self._reporters = []
        self._workingdir = workingdir
        self._global_vcomargs = globalcompargs
        if globalcompargs is None:
            self._global_vcomargs = []

    def register_reporter(self, reporter):
        """Adds a reporter to internal list of reporters, which
        will be notified of results of actions of the object
        """
        self._reporters.append(reporter)

    def map_extra_lib(self, lib_path):
        """Create a mapping to a pre-compiled library located at lib_path."""
        library_name = splitext(basename(lib_path))[0]
        self._map_library(library_name=library_name,
                          physical_name=lib_path)

    def compile(self, source_files):
        """Compiles a list of source_files into libraries"""
        libraries = get_source_file_libraries(source_files)
        self._create_libraries(libraries)
        self._map_libraries(libraries)
        self._compile(source_files)

    def finalize_reports(self):
        """Trigger all registered reporters to generate final summary
        reports
        """
        for reporter in self._reporters:
            reporter.finalize_report()

    def _notify_reporters(self, action_name, run_process):
        """Notify all reporters of an action taken, provided
        a name to identify that action, and the process object which
        had been run"""
        result = ActionResult(action_name,
                              run_process.status,
                              run_process.runtime,
                              get_process_info_string(run_process))
        for reporter in self._reporters:
            reporter.report(result)

    def _create_libraries(self, libraries):
        """Create library for provided iterable libraries"""
        for library in libraries:
            self._create_library(library.name)

    def _map_libraries(self, libraries):
        """Map into active libraries for provided iterable libraries"""
        for library in libraries:
            self._map_library(library.name)

    def _compile(self, source_files):
        """Compile the provided iterable source_files into their respective
        libraries
        """
        for source_file in source_files:
            self._compile_source(source_file)

    def _create_library(self, library_name):
        """Creates a new library and modifies the library list that describes
        the mapping between logical names and library index files (*.lib)

        Return the run process
        """
        run_process = self._run_cmd(["vlib", library_name, ])
        action_name = self._action_name(run_process.command +
                                        " " + library_name)
        self._notify_reporters(action_name, run_process)
        return run_process

    def _map_library(self, library_name, physical_name=None):
        """Creates or removes library mappings and library links stored in
        library.cfg files.

        Library mapping: Logical name -> *.lib
        Library link   : Import mappings from library.cfg located in directory

        Return the run process
        """
        arg_list = ["vmap", library_name, ]

        if physical_name:
            arg_list.append(physical_name)
        run_process = self._run_cmd(arg_list)

        action = run_process.command + " " + library_name

        if physical_name:
            action += " (%s)" % (physical_name)

        action_name = self._action_name(action)
        self._notify_reporters(action_name, run_process)
        return run_process

    def _compile_source(self, source_file):
        """Compile a source file into its respective library

        Return the run process
        """
        args_base = ["vcom"]
        args_base.extend(self._global_vcomargs)

        # the there are special arguments for this file, add
        # them to the globally specified arguments
        if source_file.compilation_arguments:
            args_base.extend(source_file.compilation_arguments)

        args_base.extend(["-work", source_file.library.name,
                          source_file.name, ])
        run_process = self._run_cmd(args_base)
        action = "%s (%s) %s" % (run_process.command,
                                 source_file.library.name,
                                 basename(source_file.name))
        action_name = self._action_name(action)
        self._notify_reporters(action_name, run_process)
        return run_process

    def _run_cmd(self, args):
        """Run the command defined by args, returning the process
        that has been run"""
        proc = Process(args, self._getkwopts())
        proc.run()
        return proc

    def _getkwopts(self):
        """Get common keyword arguments dictionary to use for all
        subprocesses invocations"""
        return {"stderr": subprocess.STDOUT,
                "cwd": self._workingdir,
                "universal_newlines": True, }

    @staticmethod
    def _action_name(action):
        return "%s.%s" % (Riviera.TOOL_NAME, action)
