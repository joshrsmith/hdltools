'''
(c) 2015 Joshua Smith

Created on Aug 30, 2015
'''

import time
import string
from xml.etree import ElementTree
from sys import version_info

from vunit.test_report import PASSED, FAILED, SKIPPED


class ActionResult(object):
    """A result of an action taken"""

    def __init__(self, name, status, elapsed, info=None):
        assert status in (PASSED,
                          FAILED,
                          SKIPPED)
        self.name = name
        self._status = status
        self.elapsed = elapsed
        self.info = info

    @property
    def passed(self):
        return self._status == PASSED

    @property
    def skipped(self):
        return self._status == SKIPPED

    @property
    def failed(self):
        return self._status == FAILED

    def __str__(self):
        return "%s <%s> (%.3f sec):\n%s" %\
            (self.name, string.upper(self._status.name),
             self.elapsed, self.info)

    def to_xunit_xml(self):
        """Get an xUnit XML style representation of the result"""
        test = ElementTree.Element("testcase")
        test.attrib["name"] = self.name
        test.attrib["time"] = "%.3f" % self.elapsed
        if self.failed:
            failure = ElementTree.SubElement(test, "failure")
            failure.attrib["message"] = "Failed"
            # Include info in failure element for parsers that
            # may not dispay system-out
            if self.info:
                failure.text = self.info
        elif self.skipped:
            skipped = ElementTree.SubElement(test, "skipped")
            skipped.attrib["message"] = "Skipped"
        if self.info:
            system_out = ElementTree.SubElement(test, "system-out")
            system_out.text = self.info
        return test


class reporter(object):
    """A generic reporter collects reports of actions."""

    def __init__(self):
        self._reports = []

    def report(self, action_result):
        """Record an action result"""

        # Internally we keep track of when the result was received
        self._reports.append((time.time(), action_result))

    def _summarize(self):
        """Summarize the reported actions"""
        failures = []
        passed = []
        skipped = []
        for _, result in self._reports:
            if result.passed:
                passed.append(result)
            elif result.failed:
                failures.append(result)
            elif result.skipped:
                skipped.append(result)
        total_time = self._reports[-1][0] - self._reports[0][0]
        return passed, failures, skipped, total_time


class xunit_reporter(reporter):
    """Record actions and emit an xUnit compatible report"""

    def __init__(self, xml_filehandle):
        """xml_filehandle is an open filehandle to write the report"""
        self._fh = xml_filehandle
        super(xunit_reporter, self).__init__()

    def finalize_report(self):
        """Summarize the reports received so far by writing to the
        provided filehandle."""
        _, failures, skipped, total_time = self._summarize()

        root = ElementTree.Element("testsuite")
        root.attrib["name"] = "testsuite"
        root.attrib["errors"] = "0"
        root.attrib["failures"] = str(len(failures))
        root.attrib["skipped"] = str(len(skipped))
        root.attrib["tests"] = str(len(self._reports))
        root.attrib["time"] = "%.3f" % total_time

        for _, report in self._reports:
            root.append(report.to_xunit_xml())

        if version_info >= (3, 0):
            # Python 3.x
            xml = ElementTree.tostring(root, encoding="unicode")
        else:
            # Python 2.x
            xml = ElementTree.tostring(root, encoding="utf-8")
        self._fh.write(xml)


class print_reporter(reporter):

    def __init__(self, print_function):
        self._printfn = print_function
        super(print_reporter, self).__init__()

    def report(self, action_result):
        reporter.report(self, action_result)
        self._printfn(action_result)

    def finalize_report(self):
        success, failures, skipped, total_time = self._summarize()
        self._printfn("%d success, %d failures, %d skipped, %.3f total time" %
                      (len(success), len(failures), len(skipped), total_time))
