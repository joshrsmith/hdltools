'''
(c) 2015 Joshua Smith

Created on Aug 30, 2015
'''

from os.path import join, abspath
from os import walk
from vunit import VUnit


def get_compile_order_source_files(root_path, library_names, target=None):
    """Looking in root_path for directories listed in library_named,
    walk through each of those directories and recursively include
    the VHDL files found. Once all the files are found, return
    a list of source file objects in compilation order.
    """
    vunit_project = VUnit.from_argv(argv=[])

    for library_name in library_names:
        _add_library_files_to_vunit_project(
            vunit_project, root_path, library_name)

    if target and not abspath(target):
        target = join(root_path, target)

    return vunit_project.get_project_compile_order(target=target)


def _add_library_files_to_vunit_project(vunit_project,
                                        root_path, library_name):

    def add_vhdl_to_library_recursively(library_name):
        lib = vunit_project.add_library(library_name)
        for path, _, _, in walk(join(root_path, library_name)):
            lib.add_source_files(join(path, "*.vhd"))
            lib.add_source_files(join(path, "*.vhdl"))

    add_vhdl_to_library_recursively(library_name)
