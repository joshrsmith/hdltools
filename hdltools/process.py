'''
(c) 2015 Joshua Smith

Created on Sept 1, 2015
'''

import time
import subprocess
from subprocess import CalledProcessError
from vunit.test_report import TestStatus


class Process(object):
    """A process to run"""

    def __init__(self, args, kwoptions):
        """
        args - list of arguments, where args[0] is command to run
               and subsequent items in list are arguments
        kwoptions - Keyword options to pass to subprocess
        """
        self._args = args
        self._kwoptions = kwoptions
        self._hasrun = False
        self._returncode = None
        self._output = None
        self._runtime = None

    def run(self):
        """Run the process, returning the exit code"""
        start_time = time.clock()
        try:
            self._output = subprocess.check_output(self._args,
                                                   **self._kwoptions)
            self._returncode = 0
        except CalledProcessError as subprocess_exception:
            self._output = subprocess_exception.output
            self._returncode = subprocess_exception.returncode
        self._runtime = time.clock() - start_time
        self._hasrun = True
        return self._returncode

    @property
    def command(self):
        """The command executed by the process"""
        return self._args[0]

    @property
    def done(self):
        """Check if the process has been run"""
        return self._hasrun

    @property
    def returncode(self):
        """Get the return code of the process that was previously run"""
        return self._returncode

    @property
    def runtime(self):
        """Execution time of the process in seconds"""
        return self._runtime

    @property
    def output(self):
        """Output captured by subprocess.check_output.
        May or may not include stderr depending on keyword arugments"""
        return self._output

    @property
    def arg_string(self):
        """A string representation of the command executed"""
        return ' '.join(self._args)

    @property
    def status(self):
        """Get the TestStatus of the process"""
        if self._hasrun is False:
            return TestStatus("skipped")
        elif self._returncode == 0:
            return TestStatus("passed")
        else:
            return TestStatus("failed")
