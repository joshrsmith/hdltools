'''
(c) 2015 Joshua Smith

Created on Aug 30, 2015
'''


def get_source_file_libraries(source_files):
    """Get set of unique libraries referenced by source_files"""
    libraries = set()
    library_names = []
    for source_file in source_files:
        if source_file.library.name not in library_names:
            library_names.append(source_file.library.name)
            libraries.add(source_file.library)
    return libraries


class Library(object):
    """A library of design units."""

    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        """The logical name of the library"""
        return self._name


class SourceFile(object):
    """Contains design units."""

    def __init__(self, name, library, compargs=None):
        self._name = name
        self._library = library
        self._compargs = compargs
        if compargs is None:
            self._compargs = []

    @property
    def name(self):
        """The absolute path to the source file"""
        return self._name

    @property
    def library(self):
        """The library that source file belongs to"""
        return self._library

    @property
    def compilation_arguments(self):
        """Special compilation arguments to use for the source file"""
        return self._compargs
