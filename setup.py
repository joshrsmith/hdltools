'''
Created on Aug 30, 2015

@author: Josh
'''
import os
import ez_setup
ez_setup.use_setuptools()

from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

execfile('hdltools/_version.py')

try:
    __version__
except:
    __version__ = "0.0"

setup(
    name="hdltools",
    version=__version__,
    author="Joshua Smith",
    author_email="joshrsmith@gmail.com",
    description=("Automation tools for working with projects using"
                 "Hardware Description Languages"),
    license="MIT",
    keywords="vhdl hardware automation",
    #url = "",
    packages=find_packages(),
    long_description=read('README'),
    install_requires=['vunit-hdl'],
    entry_points={
        'console_scripts': [
            'compile_csv = compile_csv.__main__:main',
            'generate_compile_order_csv = generate_compile_order_csv.__main__:main',
        ],
    }
    #     classifiers=[
    #         "Development Status :: 3 - Alpha",
    #         "Topic :: Utilities",
    #         "License :: OSI Approved :: BSD License",
    #     ],
)
