'''
(c) 2015 Joshua Smith

Created on Aug 30, 2015
'''

import csv
import string
from os.path import join, isabs
from hdltools import project


def get_source_files_from_csv(csv_contents, csv_root):
    """Return a list of source files parsed from the provided CSV

    csv_contents - List of lines from CSV, one element per line.
                   Format is library_name, source_file_name

                   The source_file_name may be relative to csv_root
                   or absolute. In either case, the source_file
                   will internally manage it as absolute.

    csv_root     - Root directory where the csv_contents came from.
    """
    csvreader = csv.reader(csv_contents)
    source_files = []
    for row in csvreader:
        library = project.Library(name=string.strip(row[0]))
        source_filename = string.strip(row[1])

        if not isabs(source_filename):
            source_filename = join(csv_root, source_filename)

        source_file = project.SourceFile(
            name=source_filename,
            library=library,
            compargs=None)
        source_files.append(source_file)
    return source_files
