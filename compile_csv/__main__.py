'''
(c) 2015 Joshua Smith

Created on Aug 30, 2015
'''

import argparse
import os
from os.path import abspath, dirname
from hdltools import riviera
from hdltools import reporter
from hdltools import _version
from .compile_csv import get_source_files_from_csv


def printer(text):
    """Simple function to wrap the Python 2 print statement as a function"""
    print text


def main():
    """Primary entry point for compile_csv"""
    parser = argparse.ArgumentParser(
        description='Compile VHDL files in order listed in a CSV file',
        version=_version.__version__)
    parser.add_argument('--xml',
                        default=os.devnull,
                        metavar='XML_FILENAME',
                        help='xUnit style compilation results output')
    parser.add_argument('--quiet',
                        action='store_true',
                        help='Silence outputs while running')
    parser.add_argument('--vcomargs',
                        help='Comma separated global vcom arguments')
    parser.add_argument('--libs',
                        help=('Additional libraries to map - comma separated '
                              'string of path to *.lib, relative to '
                              'working_dir or absolute'))
    parser.add_argument('working_dir',
                        help='Working directory for compilation outputs')
    parser.add_argument('csv_file',
                        help=('Absolute or relative path to the input CSV'
                              ' file to process'))
    args = parser.parse_args()

    # Create a list of arguments for compiler step from provided list of
    # arguments
    vcom_arg_list = None
    if args.vcomargs:
        vcom_arg_list = [x for x in args.vcomargs.split(',')]

    extra_libs_list = []
    if args.libs:
        extra_libs_list = [lib for lib in args.libs.split(',')]

    compiler = riviera.Riviera(args.working_dir, globalcompargs=vcom_arg_list)

    with open(args.xml, 'w') as xml_fh:  # log type files
        xml_reporter = reporter.xunit_reporter(xml_filehandle=xml_fh)
        compiler.register_reporter(xml_reporter)

        if not args.quiet:
            print_reporter = reporter.print_reporter(print_function=printer)
            compiler.register_reporter(print_reporter)

        with open(args.csv_file, 'r') as csv_fh:
            csv_file_root_path = dirname(abspath(args.csv_file))
            csv_contents = csv_fh.readlines()

        source_files = get_source_files_from_csv(
            csv_contents=csv_contents,
            csv_root=csv_file_root_path)

        for lib in extra_libs_list:
            compiler.map_extra_lib(lib_path=lib)

        compiler.compile(source_files=source_files)
        compiler.finalize_reports()

if __name__ == '__main__':
    main()
